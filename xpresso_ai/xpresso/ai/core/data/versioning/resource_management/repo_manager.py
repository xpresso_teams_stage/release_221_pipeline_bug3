from xpresso.ai.core.data.versioning.resource \
    import PachydermResource
from xpresso.ai.core.commons.exceptions.xpr_exceptions\
    import PachydermOperationException, RepoInfoException,\
    RepoPermissionException
from xpresso.ai.core.data.versioning.utils \
    import find_date_from_seconds, name_validity_check


class RepoManager(PachydermResource):
    """
    Manages repo and its related functionality on a pachyderm cluster
    """
    CREATION_DATE = "Date of creation"
    INPUT_REPO_NAME = "name"
    OUTPUT_REPO_NAME = "repo_name"

    def __init__(self, pachyderm_client):
        super().__init__()
        self.pachyderm_client = pachyderm_client

    def validate_repo(self, repo_name):
        """
        checks if the repo exists or not

        :return:
            bool: True or False
        """
        # checks if the repo name is valid or not
        name_validity_check(self.INPUT_REPO_NAME, repo_name)
        # check if the repo exists
        exist_status = False
        repo_list = self.list()
        for repo in repo_list:
            if repo[self.OUTPUT_REPO_NAME] == repo_name:
                exist_status = True
                break
        if not exist_status:
            raise RepoInfoException(f"{repo_name} repo not found")

    def create(self, repo_name: str, description: str = None):
        """
        creates a new repo on pachyderm cluster
        :param repo_name:
                name of the repo
        :param description:
                brief description of the repo
        :return:
        """
        try:
            self.validate_repo(repo_name)
            raise PachydermOperationException(f"{repo_name} repo already exists")
        except RepoInfoException:
            self.pachyderm_client.create_new_repo(repo_name, description)

    def list(self):
        """
        returns the list of all available repos

        :return:
            returns list of repos
        """
        repo_info = self.pachyderm_client.get_repo()
        return self.filter_output(repo_info)

    def delete(self, repo_name):
        """
        deletes a repo from pachyderm cluster

        This is client level operation
        :param repo_name:
            name of the repo to be deleted
        :return:
        """
        # TODO: Delete branch level info from database
        self.validate_repo(repo_name)
        self.pachyderm_client.delete_repo(repo_name)

    def filter_output(self, repo_info):
        """
        filters RepoInfo object and returns user friendly output

        :param repo_info:
            RepoInfo object
        :return:
            list of repos
        """
        repo_list = []
        for repo_item in repo_info:
            temp_repo = {}
            # repo_item is a RepoInfo object and always contain the below
            # used fields inside it unless python_pachyderm module is updated
            temp_repo[self.OUTPUT_REPO_NAME] = repo_item.repo.name
            temp_repo[self.CREATION_DATE] = find_date_from_seconds(
                repo_item.created.seconds
            )
            repo_list.append(temp_repo)
        return repo_list
